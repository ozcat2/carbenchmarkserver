# Copyright 2015 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
import json
import copy
from statistics import mean, pstdev

from django.db.models import Count, Min, Max, Q, Sum, Avg
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from app.views.helper import get_status_text


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K


def order_by_item_order(first, second, item_order):
    first_value = item_order.index(first['id']) if first['id'] in item_order else 999
    second_value = item_order.index(second['id']) if second['id'] in item_order else 999

    return first_value - second_value


class Evaluator(models.Model):
    code = models.TextField(null=True, blank=True)
    first_name = models.TextField(null=True, blank=True)
    last_name = models.TextField(null=True, blank=True)
    icon_color = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'evaluators'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {} | {} | {}'.format(self.id,
                                               self.code,
                                               self.first_name,
                                               self.last_name,
                                               self.icon_color)

    def to_list(self):
        return {
            'id': self.id,

            'code': self.code,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'icon_color': self.icon_color,

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),

            'test_total': 0,
            'test_complete': 0,
            'status': 0,
            'status_text': get_status_text(0),
        }

    @classmethod
    def get_object_list(cls):
        evaluator_list = []

        evaluators = Evaluator.objects.all()
        for evaluator in evaluators:
            evaluator_list.append(evaluator.to_list())

        return evaluator_list

    @classmethod
    def get_user_dict_by_code(cls, evaluator_code):
        evaluator = Evaluator.objects.filter(code=evaluator_code).first()
        if evaluator:
            return evaluator.to_list()

        return None

    @classmethod
    def get_user_by_code(cls, evaluator_code):
        return Evaluator.objects.filter(code=evaluator_code).first()


class Car(models.Model):
    brand = models.TextField(null=True, blank=True)
    model = models.TextField(null=True, blank=True)
    icon_color = models.TextField(null=True, blank=True)
    logo = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'cars'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {} | {} | {}'.format(self.id,
                                               self.brand,
                                               self.model,
                                               self.icon_color,
                                               self.logo)

    def to_list(self):
        return {
            'id': self.id,

            'brand': self.brand,
            'model': self.model,
            'icon_color': self.icon_color,
            'logo': self.logo,
            # TODO make logo url
            'logo_url': self.get_logo_url(),

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),

            'test_total': 0,
            'test_complete': 0,
            'status': 0,
            'status_text': get_status_text(0),
        }

    def get_logo_url(self):
        return self.logo

        if 'http' in self.logo:
            return self.logo

        return '{}{}'.format(settings.TEMP_HOST, self.logo)

    @classmethod
    def get_object_list(cls):
        car_list = []

        cars = cls.objects.all()
        for car in cars:
            car_list.append(car.to_list())

        return car_list


class Score(models.Model):
    type = models.TextField(null=True, blank=True)

    slide_type = models.TextField(null=True, blank=True)
    is_na_enable = models.BooleanField(default=False)

    name_1 = models.TextField(null=True, blank=True)
    value_1 = models.TextField(null=True, blank=True)
    name_2 = models.TextField(null=True, blank=True)
    value_2 = models.TextField(null=True, blank=True)
    name_3 = models.TextField(null=True, blank=True)
    value_3 = models.TextField(null=True, blank=True)
    name_4 = models.TextField(null=True, blank=True)
    value_4 = models.TextField(null=True, blank=True)
    name_5 = models.TextField(null=True, blank=True)
    value_5 = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'scores'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {}'.format(self.id,
                                    self.type,
                                    self.slide_type)

    def to_list(self):
        return {
            'id': self.id,

            'type': self.type,
            'slide_type': self.slide_type,
            'is_na_enable': self.is_na_enable,

            'name_1': self.name_1,
            'value_1': float(self.value_1) if self.value_1 else None,
            'name_2': self.name_2,
            'value_2': float(self.value_2) if self.value_2 else None,
            'name_3': self.name_3,
            'value_3': float(self.value_3) if self.value_3 else None,
            'name_4': self.name_4,
            'value_4': float(self.value_4) if self.value_4 else None,
            'name_5': self.name_5,
            'value_5': float(self.value_5) if self.value_5 else None,

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),
        }

    @classmethod
    def get_object_list(cls):
        score_list = []

        scores = cls.objects.all()
        for score in scores:
            score_list.append(score.to_list())

        return score_list


class Criteria(models.Model):
    name = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    # scores = models.ManyToManyField(Score,  blank=True)
    score = models.ForeignKey(Score, null=True, blank=True, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'criteria'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {} | score({})'.format(self.id,
                                                 self.name,
                                                 self.description,
                                                 self.score)

    def to_list(self):
        return {
            'id': self.id,

            'name': self.name,
            'description': self.description,

            # 'num_score': self.scores.count(),
            # 'score': self.score.to_list() if self.score else {},
            'scores': self.score.to_list() if self.score else {},

            'test_total': 0,
            'test_complete': 0,
            'status': 0,
            'status_text': get_status_text(0),

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),
        }

    @classmethod
    def get_object_list(cls):
        criteria_list = []

        criterias = cls.objects.all()
        for criteria in criterias:
            criteria_list.append(criteria.to_list())

        return criteria_list

    def get_score_list(self):
        score_list = []

        scores = self.score.all()
        for score in scores:
            score_list.append(score.to_list())

        return score_list


class Evaluation(models.Model):
    name = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    level = models.TextField(null=True, blank=True)

    criteria = models.ManyToManyField(Criteria, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'evaluations'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {} | {} | criteria({})'.format(self.id,
                                                        self.level,
                                                        self.name,
                                                        self.description,
                                                        self.criteria.count())

    def to_list(self):
        obj = {
            'id': self.id,

            'level': self.level,
            'name': self.name,
            'description': self.description,

            'num_criteria': self.criteria.count(),
            'criteria': self.get_criteria_list(),

            'test_total': 0,
            'test_complete': 0,
            'status': 0,
            'status_text': get_status_text(0),

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),
        }

        obj['criteria_order'] = [{'id': e['id']} for e in obj['criteria']]

        return obj

    @classmethod
    def get_object_list(cls):
        evaluation_list = []

        evaluations = cls.objects.all()
        for evaluation in evaluations:
            obj = evaluation.to_list()
            obj['criteria_order'] = [{'id': e['id']} for e in obj['criteria']]
            evaluation_list.append(obj)

        return evaluation_list

    def get_criteria_list(self):
        criteria_list = []

        criteria = self.criteria.all()
        for cri in criteria:
            criteria_list.append(cri.to_list())

        item_order = ItemOrder.get_item_order("evaluation", "criteria", self.id)
        criteria_list = sorted(criteria_list, key=cmp_to_key(lambda a, b: order_by_item_order(a, b, item_order)))

        return criteria_list


class Event(models.Model):
    code = models.TextField(null=True, blank=True)
    name = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    evaluations = models.ManyToManyField(Evaluation, blank=True)
    cars = models.ManyToManyField(Car, blank=True)
    evaluators = models.ManyToManyField(Evaluator, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'events'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {} | {} | evaluations({}) | cars({}) | evaluators({})'.format(self.id,
                                                                                        self.code,
                                                                                        self.name,
                                                                                        self.description,
                                                                                        self.evaluations.count(),
                                                                                        self.cars.count(),
                                                                                        self.evaluators.count())

    def get_status(self):
        test_total = self.benchmark_set.count()
        test_complete = self.benchmark_set.exclude(Q(point=None) & Q(disable=False)).count()
        status = (test_complete / test_total * 100.0) if test_total > 0 else 0
        status_text = get_status_text(status)

        return test_total, test_complete, status, status_text

    def to_list(self):
        test_total, test_complete, status, status_text = self.get_status()

        obj = {
            'id': self.id,

            'code': self.code,
            'name': self.name,
            'description': self.description,

            'evaluations': self.get_evaluation_list(),
            'num_evaluation': self.evaluations.count(),

            'cars': self.get_car_list(),
            'num_car': self.cars.count(),

            'evaluators': self.get_evaluator_list(),
            'num_evaluator': self.evaluators.count(),

            'test_total': test_total,
            'test_complete': test_complete,
            'status': status,
            'status_text': status_text,

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),
        }

        obj['evaluation_order'] = [{'id': e['id']} for e in obj['evaluations']]
        obj['car_order'] = [{'id': e['id']} for e in obj['cars']]
        obj['evaluator_order'] = [{'id': e['id']} for e in obj['evaluators']]

        return obj

    @classmethod
    def get_object_list(cls):
        event_list = []

        events = Event.objects.all()
        for event in events:
            event_list.append(event.to_list())

        return event_list

    def get_evaluator_list(self):
        evaluator_list = []

        evaluators = self.evaluators.all()
        for evaluator in evaluators:
            evaluator_list.append(evaluator.to_list())

        item_order = ItemOrder.get_item_order("event", "evaluator", self.id)
        evaluator_list = sorted(evaluator_list, key=cmp_to_key(lambda a, b: order_by_item_order(a, b, item_order)))

        return evaluator_list

    def get_evaluation_list(self):
        evaluation_list = []

        evaluations = self.evaluations.all()
        for evaluation in evaluations:
            evaluation_list.append(evaluation.to_list())

        item_order = ItemOrder.get_item_order("event", "evaluation", self.id)
        evaluation_list = sorted(evaluation_list, key=cmp_to_key(lambda a, b: order_by_item_order(a, b, item_order)))

        return evaluation_list

    def get_car_list(self):
        car_list = []

        cars = self.cars.all()
        for car in cars:
            car_list.append(car.to_list())

        item_order = ItemOrder.get_item_order("event", "car", self.id)
        car_list = sorted(car_list, key=cmp_to_key(lambda a, b: order_by_item_order(a, b, item_order)))

        return car_list

    def get_mobile_event_form_by_evaluator_code(self, evaluator_user_dict):
        car_list_base = self.get_car_list()

        event_form = self.to_list()
        event_form['evaluator'] = evaluator_user_dict

        evaluation_list = event_form.get('evaluations', [])
        for evaluation in evaluation_list:

            criteria_list = evaluation.get('criteria', [])
            for criteria in criteria_list:
                criteria['cars'] = copy.deepcopy(car_list_base)

                # TODO combine data from Benchmark table
                car_list = criteria.get('cars', [])
                for car in car_list:
                    evaluator = event_form['evaluator']
                    evaluator_id = evaluator['id']
                    event_id = event_form['id']
                    evaluation_id = evaluation['id']
                    criteria_id = criteria['id']
                    # score_id = criteria['scores']['id']
                    car_id = car['id']

                    benchmark = Benchmark.get_or_create_benchmark_from_query_id(evaluator_id,
                                                                                event_id,
                                                                                evaluation_id,
                                                                                criteria_id,
                                                                                # score_id,
                                                                                car_id)

                    car['benchmark'] = benchmark.to_mobile_list()

                    evaluator['test_total'] += 1
                    evaluator['test_complete'] += 1 if (benchmark.point is not None) else 0
                    evaluator['status'] = (evaluator['test_complete'] / evaluator['test_total']) * 100.0
                    evaluator['status_text'] = get_status_text(evaluator['status'])

                    criteria['test_total'] += 1
                    criteria['test_complete'] += 1 if (benchmark.point is not None) else 0
                    criteria['status'] = (criteria['test_complete'] / criteria['test_total']) * 100.0
                    criteria['status_text'] = get_status_text(criteria['status'])

                    evaluation['test_total'] += 1
                    evaluation['test_complete'] += 1 if (benchmark.point is not None) else 0
                    evaluation['status'] = (evaluation['test_complete'] / evaluation['test_total']) * 100.0
                    evaluation['status_text'] = get_status_text(evaluation['status'])

                    event_form['test_total'] += 1
                    event_form['test_complete'] += 1 if (benchmark.point is not None) else 0
                    event_form['status'] = (event_form['test_complete'] / event_form['test_total']) * 100.0
                    event_form['status_text'] = get_status_text(event_form['status'])

        # self.clean_benchmark_record()

        logging.debug('event_form: {}'.format(event_form))
        return event_form

    def get_admin_event_form(self):
        car_list_base = self.get_car_list()
        evaluator_list_base = self.get_evaluator_list()

        event_form = self.to_list()
        event_form['alert'] = []

        evaluation_list = event_form.get('evaluations', [])
        for evaluation in evaluation_list:
            evaluation['alert'] = []

            criteria_list = evaluation.get('criteria', [])
            for criteria in criteria_list:

                # Assign Cars to Criteria
                criteria['cars'] = copy.deepcopy(car_list_base)

                # TODO combine data from Benchmark table
                car_list = criteria.get('cars', [])
                for car in car_list:
                    car['evaluators'] = []

                    car_point_sum = 0
                    car_min = None
                    car_max = None
                    car_avg = None
                    car_total = 0

                    # evaluator_list = event_form.get('evaluators', [])
                    evaluator_list = copy.deepcopy(evaluator_list_base)
                    for evaluator in evaluator_list:
                        event_id = event_form['id']
                        evaluation_id = evaluation['id']
                        criteria_id = criteria['id']
                        # score_id = criteria['scores']['id']
                        car_id = car['id']
                        evaluator_id = evaluator['id']

                        benchmark = Benchmark.get_or_create_benchmark_from_query_id(evaluator_id,
                                                                                    event_id,
                                                                                    evaluation_id,
                                                                                    criteria_id,
                                                                                    # score_id,
                                                                                    car_id)

                        evaluator['benchmark'] = benchmark.to_list()

                        evaluator['test_total'] += 1
                        evaluator['test_complete'] += 1 if (benchmark.point is not None) else 0
                        evaluator['status'] = (evaluator['test_complete'] / evaluator['test_total']) * 100.0
                        evaluator['status_text'] = get_status_text(evaluator['status'])

                        criteria['test_total'] += 1
                        criteria['test_complete'] += 1 if (benchmark.point is not None) else 0
                        criteria['status'] = (criteria['test_complete'] / criteria['test_total']) * 100.0
                        criteria['status_text'] = get_status_text(criteria['status'])

                        evaluation['test_total'] += 1
                        evaluation['test_complete'] += 1 if (benchmark.point is not None) else 0
                        evaluation['status'] = (evaluation['test_complete']/evaluation['test_total']) * 100.0
                        evaluation['status_text'] = get_status_text(evaluation['status'])

                        event_form['test_total'] += 1
                        event_form['test_complete'] += 1 if (benchmark.point is not None) else 0
                        event_form['status'] = (event_form['test_complete'] / event_form['test_total']) * 100.0
                        event_form['status_text'] = get_status_text(event_form['status'])

                        car['evaluators'].append(evaluator)

                        if (benchmark.disable == False and benchmark.point):
                            point = float(benchmark.point)
                            car_point_sum += point
                            car_total += 1
                            car_min = min(car_min, point) if car_min is not None else point
                            car_max = max(car_max, point) if car_max is not None else point
                            car_avg = float("{0:.2f}".format(car_point_sum / car_total))

                    car['car_point_sum'] = car_point_sum
                    car['car_total'] = car_total
                    car['car_min'] = car_min if car_min else 'n/a'
                    car['car_max'] = car_max if car_max else 'n/a'
                    car['car_avg'] = car_avg if car_avg else 'n/a'

                    evaluation['alert'] += self.get_list_of_evaluations_out_of_range_SD(evaluator_list)
                    evaluation['alert'] = list({v['evaluator']['id']:v for v in evaluation['alert']}.values())

                    event_form['alert'] += self.get_list_of_evaluations_out_of_range_SD(evaluator_list)
                    event_form['alert'] = list({v['evaluator']['id']:v for v in event_form['alert']}.values())

                    # Make fake evaluator for Average Field
                    evaluator_average = {
                        'code': 'Average',
                        'first_name': '',
                        'last_name': '',
                        'benchmark': {}
                    }
                    event_id = event_form['id']
                    evaluation_id = evaluation['id']
                    criteria_id = criteria['id']
                    car_id = car['id']

                    benchmark = Benchmark.objects.filter(event_id=event_id).filter(evaluation_id=evaluation_id).filter(
                        criteria_id=criteria_id).filter(car_id=car_id).exclude(point=None).exclude(disable=True).values('point').aggregate(
                        point_avg=Avg('point'))
                    point_avg = float(benchmark['point_avg']) if benchmark['point_avg'] else 0
                    evaluator_average['benchmark']['point'] = float("{0:.2f}".format(point_avg))

                    car['evaluators'].append(evaluator_average)
                    car['radar'] = float(point_avg) / 1.0

                # Assign Evaluators to Criteria
                criteria['evaluators'] = copy.deepcopy(evaluator_list_base)

                # TODO combine data from Benchmark table
                evaluator_list = criteria.get('evaluators', [])
                for evaluator in evaluator_list:
                    evaluator['cars'] = []

                    evaluator_point_sum = 0
                    evaluator_min = None
                    evaluator_max = None
                    evaluator_avg = 0
                    evaluator_total = 0

                    # car_list = event_form.get('cars', [])
                    car_list = copy.deepcopy(car_list_base)
                    for car in car_list:
                        event_id = event_form['id']
                        evaluation_id = evaluation['id']
                        criteria_id = criteria['id']
                        # score_id = criteria['scores']['id']
                        car_id = car['id']
                        evaluator_id = evaluator['id']

                        benchmark = Benchmark.get_or_create_benchmark_from_query_id(evaluator_id,
                                                                                    event_id,
                                                                                    evaluation_id,
                                                                                    criteria_id,
                                                                                    # score_id,
                                                                                    car_id)

                        car['benchmark'] = benchmark.to_list()

                        car['test_total'] += 1
                        car['test_complete'] += 1 if (benchmark.point is not None) else 0
                        car['status'] = (car['test_complete'] / car['test_total']) * 100.0
                        car['status_text'] = get_status_text(car['status'])

                        # criteria['test_total'] += 1
                        # criteria['test_complete'] += 1 if (benchmark.point is not None) else 0
                        # criteria['status'] = (criteria['test_complete'] / criteria['test_total']) * 100.0
                        # criteria['status_text'] = get_status_text(criteria['status'])
                        #
                        # evaluation['test_total'] += 1
                        # evaluation['test_complete'] += 1 if (benchmark.point is not None) else 0
                        # evaluation['status'] = (evaluation['test_complete'] / evaluation['test_total']) * 100.0
                        # evaluation['status_text'] = get_status_text(evaluation['status'])
                        #
                        # event_form['test_total'] += 1
                        # event_form['test_complete'] += 1 if (benchmark.point is not None) else 0
                        # event_form['status'] = (event_form['test_complete'] / event_form['test_total']) * 100.0
                        # event_form['status_text'] = get_status_text(event_form['status'])

                        evaluator['cars'].append(car)

                        if (benchmark.disable == False and benchmark.point):
                            point = float(benchmark.point)
                            evaluator_point_sum += point
                            evaluator_total += 1
                            evaluator_min = min(evaluator_min, point) if evaluator_min is not None else point
                            evaluator_max = max(evaluator_max, point) if evaluator_max is not None else point
                            evaluator_avg = float("{0:.2f}".format(evaluator_point_sum / evaluator_total))

                    evaluator['evaluator_point_sum'] = evaluator_point_sum
                    evaluator['evaluator_total'] = evaluator_total
                    evaluator['evaluator_min'] = evaluator_min
                    evaluator['evaluator_max'] = evaluator_max
                    evaluator['evaluator_avg'] = evaluator_avg

        # self.clean_benchmark_record()

        for evaluator in event_form.get('evaluators', []):
            event_id = self.id
            evaluator_id = evaluator['id']
            test_total = Benchmark.objects.filter(event_id=event_id).filter(evaluator_id=evaluator_id).count()
            test_complete = Benchmark.objects.filter(event_id=event_id).filter(evaluator_id=evaluator_id).exclude(Q(point=None) & Q(disable=False)).count()
            status = (test_complete/test_total) * 100.0 if test_total > 0 else 0
            status_text = get_status_text(status)

            evaluator['test_total'] = test_total
            evaluator['test_complete'] = test_complete
            evaluator['status'] = status
            evaluator['status_text'] = status_text

        logging.debug('event_form: {}'.format(event_form))
        return event_form

    def get_list_of_evaluations_out_of_range_SD(self, evaluator_list):
        alert_list = []

        x_list = []
        for evaluator in evaluator_list:
            point = evaluator.get('benchmark', {}).get('point', None)
            if (point and point != 'n/a' and point != 'i/p'):
                x_list.append(float(point))

        if len(x_list) > 0:
            pst = pstdev(x_list)
            m = mean(x_list)
            lntl = m - pst
            untl = m + pst

            for evaluator in evaluator_list:
                point = evaluator.get('benchmark', {}).get('point', None)
                if (point and point != 'n/a' and point != 'i/p'):
                    point = float(point)
                    if point < lntl or point > untl:
                        alert_list.append({'evaluator': evaluator, 'value': {'point': point, 'mean': m, 'sigma': pst, 'lntl': lntl, 'untl': untl}})
                else:
                    alert_list.append({'evaluator': evaluator,
                                       'value': {'point': point, 'mean': m, 'sigma': pst, 'lntl': lntl, 'untl': untl}})


        return alert_list

    def clean_benchmark_record(self):
        benchmarks = self.benchmark_set.all()
        for benchmark in benchmarks:
            event_id = benchmark.event_id
            evaluation_id = benchmark.evaluation_id
            criteria_id = benchmark.criteria_id
            evaluator_id = benchmark.evaluator_id
            car_id = benchmark.car_id

            print(event_id, evaluation_id, criteria_id, evaluator_id, car_id, benchmark)

            events = Event.objects.filter(id=event_id)
            # print('events', events)
            if events.exists() is False:
                # TODO Delete benchmark
                benchmark.delete()
                continue

            event = events.first()

            evaluations = event.evaluations.filter(id=evaluation_id)
            # print('evaluations', evaluations)
            if evaluations.exists() is False:
                # TODO Delete benchmark
                benchmark.delete()
                continue

            criterias = evaluations.filter(criteria__id=criteria_id)
            # print('criterias', criterias)
            if criterias.exists() is False:
                # TODO Delete benchmark
                benchmark.delete()
                continue

            cars = event.cars.filter(id=car_id)
            # print('cars', cars)
            if cars.exists() is False:
                # TODO Delete benchmark
                benchmark.delete()
                continue

            evaluators = event.evaluators.filter(id=evaluator_id)
            # print('evaluators', evaluators)
            if evaluators.exists() is False:
                # TODO Delete benchmark
                benchmark.delete()
                continue

    @classmethod
    def get_mobile_event_form_by_event_id(cls, event_id, evaluator_code=None, evaluator_id=None):
        evaluator = None
        if evaluator_id:
            evaluator = Evaluator.objects.filter(id=evaluator_id).first()
        elif evaluator_code:
            evaluator = Evaluator.objects.filter(code=evaluator_code).first()

        event = Event.objects.filter(id=event_id).filter(evaluators=evaluator).first()

        logging.debug('evaluator: {}'.format(evaluator))
        logging.debug('event: {}'.format(event))
        if event:
            evaluator_user_dict = evaluator.to_list()
            event_form = event.get_mobile_event_form_by_evaluator_code(evaluator_user_dict)

            return event_form

        return {}

    @classmethod
    def get_admin_event_form_by_event_id(cls, event_id):
        event = Event.objects.filter(id=event_id).first()
        logging.debug('event: {}'.format(event))
        if event:
            event_form = event.get_admin_event_form()

            return event_form

        return {}


class Benchmark(models.Model):
    event = models.ForeignKey(Event, null=True, blank=True, on_delete=models.CASCADE)
    car = models.ForeignKey(Car, null=True, blank=True, on_delete=models.CASCADE)
    evaluator = models.ForeignKey(Evaluator, null=True, blank=True, on_delete=models.CASCADE)

    evaluation = models.ForeignKey(Evaluation, null=True, blank=True, on_delete=models.CASCADE)
    criteria = models.ForeignKey(Criteria, null=True, blank=True, on_delete=models.CASCADE)
    # score = models.ForeignKey(Score, null=True, blank=True, on_delete=models.CASCADE)

    point = models.TextField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    disable = models.BooleanField(default=False)

    # status = models.TextField(null=True, blank=True, default='in_progress')

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'benchmarks'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {} | {} | {} | {} | {} | {} | {}'.format(self.id,
                                                                             self.event.code,
                                                                             self.event.name,
                                                                             self.evaluation.name,
                                                                             self.car.model,
                                                                             self.evaluator.code,
                                                                             self.criteria.name,
                                                                             # self.score.type,
                                                                             self.point,
                                                                             self.comment)
                                                                             # self.status)

    def to_list(self):
        point = self.point
        disable = self.disable
        if disable:
            point = 'n/a'
        elif point:
            point = float(point)
        elif point is None:
            point = 'i/p'

        return {
            'id': self.id,

            'point': point,
            'disable': disable,
            'comment': self.comment,
            # 'status': self.status,

            'event_id': self.event.id if self.event else None,
            'car_id': self.car.id if self.car else None,
            'evaluator_id': self.evaluator.id if self.evaluator else None,
            'evaluation_id': self.evaluation.id if self.evaluation else None,
            'criteria_id': self.criteria.id if self.criteria else None,
            'score_id': self.criteria.score.id if self.criteria.score else None,

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),
        }

    def to_mobile_list(self):
        point = self.point
        disable = self.disable
        if point:
            point = float(point)

        return {
            'id': self.id,

            'point': point,
            'disable': disable,
            'comment': self.comment,
            # 'status': self.status,

            'event_id': self.event.id if self.event else None,
            'car_id': self.car.id if self.car else None,
            'evaluator_id': self.evaluator.id if self.evaluator else None,
            'evaluation_id': self.evaluation.id if self.evaluation else None,
            'criteria_id': self.criteria.id if self.criteria else None,
            'score_id': self.criteria.score.id if self.criteria.score else None,

            'created': self.created_at.strftime('%s'),
            'created_text': self.created_at.strftime('%d-%m-%y'),
            'updated': self.modified_at.strftime('%s'),
            'updated_text': self.modified_at.strftime('%d-%m-%y'),
        }

    @classmethod
    def get_or_create_benchmark_from_query_id(cls, evaluator_id, event_id, evaluation_id, criteria_id, car_id): #score_id
        object = {
            'evaluator_id': evaluator_id,
            'event_id': event_id,
            'evaluation_id': evaluation_id,
            'criteria_id': criteria_id,
            # 'score_id': score_id,
            'car_id': car_id,
            # 'status': 'in_progress'
        }
        logging.debug('get_or_create_benchmark_from_query_id: {}'.format(object))

        benchmark, created = cls.objects.get_or_create(evaluator_id=evaluator_id,
                                                       event_id=event_id,
                                                       evaluation_id=evaluation_id,
                                                       criteria_id=criteria_id,
                                                       # score_id=score_id,
                                                       car_id=car_id,
                                                       defaults=object)
        # created == True -> New creation
        # created == False -> Existing one

        logging.debug('get_or_create_benchmark_from_query_id: {}, {}'.format(benchmark, created))

        return benchmark


class ItemOrder(models.Model):
    parent = models.TextField(null=True, blank=True)
    child = models.TextField(null=True, blank=True)
    parent_id = models.TextField(null=True, blank=True)
    child_id = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'item_orders'
        ordering = ['id']

    def __str__(self):
        return '{} | {} | {} | {}'.format(self.id,
                                         self.parent,
                                         self.child,
                                         self.order)

    def get_order_list(self):
        order_list = []

        if self.child_id:
            order_list = [int(e) for e in self.child_id.split(',')]

        return order_list

    def get_json_order_list(self):
        order_list = []

        if self.child_id:
            order_list = json.loads(self.child_id)

        logging.debug('get_json_order_list: {}'.format(order_list))

        return order_list

    @classmethod
    def create_or_update_item_order(cls, parent, child, parent_id, child_id):
        child_id = [e['id'] for e in child_id]

        logging.debug('create_or_update_item_order: {}, {}, {}, {}'.format(parent, child, parent_id, child_id))
        item_order = ItemOrder.objects.filter(parent=parent).filter(child=child).filter(parent_id=parent_id).first()
        if item_order:
            logging.debug('create_or_update_item_order: {}, {}, {}, {}, --update'.format(parent, child, parent_id, child_id))
            item_order.child_id = json.dumps(child_id)
            item_order.save()
        else:
            logging.debug('create_or_update_item_order: {}, {}, {}, {}, --create'.format(parent, child, parent_id, child_id))
            item_order = ItemOrder.objects.create(parent=parent, child=child, parent_id=parent_id, child_id=json.dumps(child_id))

        return item_order

    @classmethod
    def delete_item_order(cls, parent, child, parent_id):
        logging.debug('delete_item_order: {}, {}, {}'.format(parent, child, parent_id))
        item_order = ItemOrder.objects.filter(parent=parent).filter(child=child).filter(parent_id=parent_id).first()
        if item_order:
            logging.debug('delete_item_order: {}, {}, {} --delete'.format(parent, child, parent_id))
            item_order.delete()
        else:
            logging.debug('delete_item_order: {}, {}, {} --delete item not found'.format(parent, child, parent_id))

        return item_order

    @classmethod
    def get_item_order(cls, parent, child, parent_id):
        logging.debug('get_item_order: {}, {}, {}'.format(parent, child, parent_id))
        item_order = ItemOrder.objects.filter(parent=parent).filter(child=child).filter(parent_id=parent_id).first()
        if item_order:
            logging.debug('get_item_order: {}, {}, {} --get'.format(parent, child, parent_id))
            item_order = item_order.get_json_order_list()
        else:
            logging.debug('get_item_order: {}, {}, {} --get item not found'.format(parent, child, parent_id))
            item_order = []

        return item_order

    @classmethod
    def get_item_order_under_id(cls, parent, child, parent_id):
        item_order = ItemOrder.get_item_order(parent, child, parent_id)

        return [{'id': e} for e in item_order]
