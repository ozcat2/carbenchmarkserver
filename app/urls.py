# Copyright 2015 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from django.conf.urls import url
from django.conf import settings

from app.views.mobile_event import MobileEvent
from app.views.admin_evaluator import AdminEvaluator
from app.views.admin_car import AdminCar
from app.views.admin_score import AdminScore
from app.views.admin_criteria import AdminCriteria
from app.views.admin_evaluation import AdminEvaluation
from app.views.admin_event import AdminEvent
from app.views.admin_login import AdminLogin
from app.views.upload import simple_upload

from app.views.report_summary import CSVSummary
from app.views.report_marker import CSVMarker
from app.views.report_candle import CSVCandle
from app.views.report_radar import ReportRadar, CSVRadar
from app.views.report_vehicle import ReportVehicle, CSVVehicle


app_name = 'app'

urlpatterns = [
    url(r'^login/?', AdminLogin.as_view()),

    url(r'^mobile/event/?', MobileEvent.as_view()),
    url(r'^evaluator/?', AdminEvaluator.as_view()),
    url(r'^car/?', AdminCar.as_view()),
    url(r'^score/?', AdminScore.as_view()),
    url(r'^criteria/?', AdminCriteria.as_view()),
    url(r'^evaluation/?', AdminEvaluation.as_view()),
    url(r'^event/?', AdminEvent.as_view()),

    url(r'^report/summary/?', AdminEvent.as_view()),
    url(r'^report/marker/?', AdminEvent.as_view()),
    url(r'^report/candle/?', AdminEvent.as_view()),
    url(r'^report/radar/?', ReportRadar.as_view()),
    url(r'^report/vehicle/?', ReportVehicle.as_view()),

    url(r'^csv/summary/?', CSVSummary.as_view()),
    url(r'^csv/marker/?', CSVMarker.as_view()),
    url(r'^csv/candle/?', CSVCandle.as_view()),
    url(r'^csv/radar/?', CSVRadar.as_view()),
    url(r'^csv/vehicle/?', CSVVehicle.as_view()),

    url(r'^upload/?', simple_upload),
]
