import logging
import traceback
import json

from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from app.views.helper import is_admin_access
from app.models import Car


BASE_MODEL = Car


class AdminCar(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminCar, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        object_list = BASE_MODEL.get_object_list()
        response = {'data': object_list}

        logging.debug('response: {0}'.format(response))
        return JsonResponse(response)

    def post(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        response = self.create_or_update_item('post')

        logging.debug('response: {0}'.format(response))
        return response

    def put(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        response = self.create_or_update_item('put')

        logging.debug('response: {0}'.format(response))
        return response

    def delete(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        id = self.request.GET.get('id', None)

        exist_item = BASE_MODEL.objects.filter(id=id).first()
        if exist_item is None:
            return HttpResponse('item not found', status=400)

        exist_item.delete()

        response = HttpResponse('success', status=200)

        logging.debug('response: {0}'.format(response))
        return response

    def create_or_update_item(self, method):
        try:
            params = json.loads(self.request.body)
            id = params.get('id', None)
            brand = params.get('brand', '')
            model = params.get('model', '')
            icon_color = params.get('icon_color', '')
            logo = params.get('logo', '')
        except:
            traceback.print_exc()
            return HttpResponse('invalid parameter', status=400)

        exist_item = BASE_MODEL.objects.filter(brand=brand).filter(model=model).first()

        if method == 'post':
            if exist_item:
                return HttpResponse('item already exist', status=400)

            item = BASE_MODEL.objects.create(brand=brand, model=model, icon_color=icon_color, logo=logo)
            if item:
                return HttpResponse('success', status=201)

        elif method == 'put' and id:
            if exist_item:
                if exist_item.id != id:
                    return HttpResponse('item already exist', status=400)

            item = BASE_MODEL.objects.filter(id=id).first()
            if item is None:
                return HttpResponse('item not found', status=400)

            item.brand = brand
            item.model = model
            item.icon_color = icon_color
            item.logo = logo
            item.save()

            return HttpResponse('success', status=200)

        return HttpResponse('invalid request', status=400)
