import logging
import json
import traceback

from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from app.views.helper import is_admin_access
from app.models import Event, Evaluation, Evaluator, Car, ItemOrder


BASE_MODEL = Event


class AdminEvent(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminEvent, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        # Get Query String
        event_id = request.GET.get('event_id', None)
        evaluator_id = request.GET.get('evaluator_id', None)
        event_id = event_id if event_id != '' else None
        evaluator_id = evaluator_id if evaluator_id != '' else None

        response = {}

        if (event_id is None) and (evaluator_id is None):
            object_list = BASE_MODEL.get_object_list()
            response = {'data': object_list}

        elif event_id and evaluator_id:
            response = BASE_MODEL.get_mobile_event_form_by_event_id(event_id=event_id, evaluator_id=evaluator_id)

        elif event_id:
            response = BASE_MODEL.get_admin_event_form_by_event_id(event_id)

        logging.debug('response: {0}'.format(response))
        return JsonResponse(response)

    def post(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        response = self.create_or_update_item('post')

        logging.debug('response: {0}'.format(response))
        return response

    def put(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        response = self.create_or_update_item('put')

        logging.debug('response: {0}'.format(response))
        return response

    def delete(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        id = self.request.GET.get('id', None)

        exist_item = BASE_MODEL.objects.filter(id=id).first()
        if exist_item is None:
            return HttpResponse('item not found', status=400)

        exist_item.delete()

        ItemOrder.delete_item_order('event', 'evaluation', exist_item.id)
        ItemOrder.delete_item_order('event', 'evaluator', exist_item.id)
        ItemOrder.delete_item_order('event', 'car', exist_item.id)

        response = HttpResponse('success', status=200)

        logging.debug('response: {0}'.format(response))
        return response

    def create_or_update_item(self, method):
        try:
            params = json.loads(self.request.body)
            id = params.get('id', None)
            code = params.get('code', None)
            name = params.get('name', '')
            description = params.get('description', '')

            evaluations = params.get('evaluations', [])
            evaluators = params.get('evaluators', [])
            cars = params.get('cars', [])

            evaluation_order = params.get('evaluation_order', [])
            evaluator_order = params.get('evaluator_order', [])
            car_order = params.get('car_order', [])

        except:
            traceback.print_exc()
            return HttpResponse('invalid parameter', status=400)

        exist_item = BASE_MODEL.objects.filter(code=code).first()

        if method == 'post':
            if exist_item:
                return HttpResponse('item already exist', status=400)

            item = BASE_MODEL.objects.create(code=code, name=name, description=description)
            if item:
                for evaluation in evaluations:
                    child = Evaluation.objects.filter(id=evaluation['id']).first()
                    if child:
                        item.evaluations.add(child)
                # item.save()

                for evaluator in evaluators:
                    child = Evaluator.objects.filter(id=evaluator['id']).first()
                    if child:
                        item.evaluators.add(child)
                # item.save()

                for car in cars:
                    child = Car.objects.filter(id=car['id']).first()
                    if child:
                        item.cars.add(child)

                item.save()

                ItemOrder.create_or_update_item_order('event', 'evaluation', item.id, evaluation_order)
                ItemOrder.create_or_update_item_order('event', 'evaluator', item.id, evaluator_order)
                ItemOrder.create_or_update_item_order('event', 'car', item.id, car_order)

                # TODO Generate Benchmark Transaction Records
                item.get_admin_event_form()
                item.clean_benchmark_record()

                return HttpResponse('success', status=201)

        elif method == 'put' and id:
            if exist_item:
                if exist_item.id != id:
                    return HttpResponse('item already exist', status=400)

            item = BASE_MODEL.objects.filter(id=id).first()
            if item is None:
                return HttpResponse('item not found', status=400)

            item.code = code
            item.name = name
            item.description = description

            item.evaluations.clear()
            item.evaluators.clear()
            item.cars.clear()
            item.save()

            if item:
                for evaluation in evaluations:
                    child = Evaluation.objects.filter(id=evaluation['id']).first()
                    if child:
                        item.evaluations.add(child)
                # item.save()

                for evaluator in evaluators:
                    child = Evaluator.objects.filter(id=evaluator['id']).first()
                    if child:
                        item.evaluators.add(child)
                # item.save()

                for car in cars:
                    child = Car.objects.filter(id=car['id']).first()
                    if child:
                        item.cars.add(child)

                item.save()

                ItemOrder.create_or_update_item_order('event', 'evaluation', item.id, evaluation_order)
                ItemOrder.create_or_update_item_order('event', 'evaluator', item.id, evaluator_order)
                ItemOrder.create_or_update_item_order('event', 'car', item.id, car_order)

                # TODO Generate Benchmark Transaction Records
                item.get_admin_event_form()
                item.clean_benchmark_record()

            return HttpResponse('success', status=200)

        return HttpResponse('invalid request', status=400)
