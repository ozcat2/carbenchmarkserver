import json
import logging
import traceback

from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token


class AdminLogin(generic.View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminLogin, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logging.debug('request {0}'.format(self.request.body))

        try:
            params = json.loads(self.request.body)
            username = params['username']
            password = params['password']

            user = User.objects.get(username=username)
            if not user.check_password(password):
                raise Exception('invalid password')
            logging.debug('login success')

            token = Token.objects.filter(user=user).first()
            if not token:
                token = Token.objects.create(user=user)
                logging.debug('create new user token {0}'.format(token))
            logging.debug('user credential {0} {1}'.format(username, token))
        except:
            traceback.print_exc()

            return HttpResponse('invalid credential', status=400)

        response = {
            'access_token': token.key,
            'username': user.username,
            'role': 'admin'
        }

        logging.debug('response {0}'.format(response))
        return JsonResponse(response)
