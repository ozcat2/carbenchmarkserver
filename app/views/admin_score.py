import logging
import traceback
import json

from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from app.views.helper import is_admin_access
from app.models import Score


BASE_MODEL = Score


class AdminScore(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AdminScore, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        object_list = BASE_MODEL.get_object_list()
        response = {'data': object_list}

        logging.debug('response: {0}'.format(response))
        return JsonResponse(response)

    def post(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        response = self.create_or_update_item('post')

        logging.debug('response: {0}'.format(response))
        return response

    def put(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        response = self.create_or_update_item('put')

        logging.debug('response: {0}'.format(response))
        return response

    def delete(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        id = self.request.GET.get('id', None)

        exist_item = BASE_MODEL.objects.filter(id=id).first()
        if exist_item is None:
            return HttpResponse('item not found', status=400)

        exist_item.delete()

        response = HttpResponse('success', status=200)

        logging.debug('response: {0}'.format(response))
        return response

    def create_or_update_item(self, method):
        try:
            params = json.loads(self.request.body)
            id = params.get('id', None)
            type = params.get('type', '')
            # is_na_enable = True if params.get('is_na_enable', 'false').lower() == 'true' else False
            is_na_enable = params.get('is_na_enable', False)
            name_1 = params.get('name_1', None)
            value_1 = params.get('value_1', None)
            name_2 = params.get('name_2', None)
            value_2 = params.get('value_2', None)
            name_3 = params.get('name_3', None)
            value_3 = params.get('value_3', None)
            name_4 = params.get('name_4', None)
            value_4 = params.get('value_4', None)
            name_5 = params.get('name_5', None)
            value_5 = params.get('value_5', None)

            # slide_type = params.get('slide_type', 'int')
            slide_type = 'float' if (value_5 and float(value_5) <= 10.0) else 'int'

        except:
            traceback.print_exc()
            return HttpResponse('invalid parameter', status=400)

        exist_item = BASE_MODEL.objects.filter(type=type).first()

        if method == 'post':
            if exist_item:
                return HttpResponse('item already exist', status=400)

            item = BASE_MODEL.objects.create(type=type, slide_type=slide_type, is_na_enable=is_na_enable, name_1=name_1, name_2=name_2, name_3=name_3, name_4=name_4, name_5=name_5, value_1=value_1, value_2=value_2, value_3=value_3, value_4=value_4, value_5=value_5)
            if item:
                return HttpResponse('success', status=201)

        elif method == 'put' and id:
            if exist_item:
                if exist_item.id != id:
                    return HttpResponse('item already exist', status=400)

            item = BASE_MODEL.objects.filter(id=id).first()
            if item is None:
                return HttpResponse('item not found', status=400)

            item.type = type
            item.slide_type = slide_type
            item.is_na_enable = is_na_enable
            item.name_1 = name_1
            item.value_1 = value_1
            item.name_2 = name_2
            item.value_2 = value_2
            item.name_3 = name_3
            item.value_3 = value_3
            item.name_4 = name_4
            item.value_4 = value_4
            item.name_5 = name_5
            item.value_5 = value_5
            item.save()

            return HttpResponse('success', status=200)

        return HttpResponse('invalid request', status=400)
