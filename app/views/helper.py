import hashlib
from datetime import datetime

from django.utils import timezone
from django.conf import settings


def get_response_from_template(success=False, message='', data={}, status_code=200):
    response = {
        'success': success,
        'message': message,
        'data': data
    }

    return response, status_code


def get_something_went_wrong_response():
    return get_response_from_template(message='something went wrong')


def get_unauthorized_response():
    return get_response_from_template(message='unauthorized')


def get_user_not_found_response():
    return get_response_from_template(message='user not found')


def get_method_not_allow_response():
    return get_response_from_template(message='method not allow')


def get_incorrect_format_response():
    return get_response_from_template(message='incorrect format')


def get_user_exist_response():
    return get_response_from_template(message='user already existed')


def get_if_not_none_and_empty(obj):
    if (obj) and (obj != ''):
        return obj

    return None


def generate_base64(prefix='', digit=6):
    hash = hashlib.sha1()
    hash.update(datetime.now(tz=timezone.utc).strftime('%Y-%m-%d %H:%M:%S.%f'))
    hash_text = '{0}{1}'.format(prefix, hash.digest().encode('base64')[:digit])

    return hash_text


def generate_hex(prefix='', digit=6, upper_case=True):
    hash = hashlib.sha1()
    hash.update(datetime.now(tz=timezone.utc).strftime('%Y-%m-%d %H:%M:%S.%f').encode('utf-8'))
    hash_text = '{0}{1}'.format(prefix, hash.hexdigest()[:digit])
    if upper_case:
        hash_text = hash_text.upper()

    return hash_text


def get_datetime_range(dt1, dt2=None):
    dt1, dt2 = get_start_and_end_date(dt1, dt2)
    dt__range = (dt1, dt2)

    return dt__range


def get_start_and_end_date(dt1, dt2=None):
    dt2 = dt2 if dt2 else dt1
    if dt1 > dt2:
        dt1, dt2 = dt2, dt1
    dt1 = get_datetime_start_of_day(dt1)
    dt2 = get_datetime_end_of_day(dt2)

    return dt1, dt2


def get_datetime_start_of_day(dt):
    return datetime.combine(dt, datetime.min.time())


def get_datetime_end_of_day(dt):
    return datetime.combine(dt, datetime.max.time())


def get_value_from_params(request, key):
    value = request.GET.get(key, None)
    if value and value != '':
        return value

    return None


def convert_datetime_to_timestamp(dt):
    ts = ''
    try:
        ts = dt.strftime('%s')
    except:
        pass

    return ts


def convert_datetime_to_readable(dt):
    rd = ''
    try:
        rd = dt.strftime('%Y-%m-%d %H:%M:%S')
    except:
        pass

    return rd


def unique_order(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


def unique_un_order(sequence):
    return list(set(sequence))


def is_admin_access(request):
    if settings.NEED_AUTH is False:
        return True

    from rest_framework.authtoken.models import Token
    token_header = request.META.get('HTTP_AUTHORIZATION', None)
    token_query_string = request.GET.get('token', None)

    return Token.objects.filter(key__in=[token_header, token_query_string]).exists()


def get_status_text(status):
    status = int(status)
    if status == 100:
        return 'Completed'

    return '{} %'.format(status)
