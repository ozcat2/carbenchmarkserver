import logging
import json
import traceback

from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from app.models import Event, Benchmark


class MobileEvent(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(MobileEvent, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        # Get Query String
        event_id = request.GET.get('event_id', None)
        evaluator_code = request.GET.get('evaluator_code', None)
        event_id = event_id if event_id != '' else None
        evaluator_code = evaluator_code if evaluator_code != '' else None

        response = {}

        if (event_id is None) and (evaluator_code is None):
            event_list = Event.get_object_list()
            response = {'data': event_list}

        elif event_id and evaluator_code:
            response = Event.get_mobile_event_form_by_event_id(event_id=event_id, evaluator_code=evaluator_code)

            if response == {}:
                logging.debug('response: {0}'.format(response))
                return HttpResponse('unauthorized', status=401)

        logging.debug('response: {0}'.format(response))
        return JsonResponse(response)

    def put(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.body))

        benchmark_update_list = []
        try:
            event = json.loads(self.request.body)
            evaluations = event.get('evaluations', [])
            for evaluation in evaluations:
                criteria = evaluation.get('criteria', [])
                for cri in criteria:
                    cars = cri.get('cars', [])
                    for car in cars:
                        benchmark = car.get('benchmark', [])
                        benchmark_update_list.append(benchmark)
        except:
            traceback.print_exc()
            return HttpResponse('invalid parameter', status=400)

        for benchmark_update in benchmark_update_list:
            benchmark = Benchmark.objects.filter(id=benchmark_update['id']).first()
            if benchmark:
                point = benchmark_update.get('point', None)
                comment = benchmark_update.get('comment', None)
                # disable = True if benchmark_update.get('disable', 'false').lower() == 'true' else False
                disable = benchmark_update.get('disable', False)

                if point:
                    benchmark.point = point

                if comment:
                    benchmark.comment = comment

                benchmark.disable = disable

                benchmark.save()

        logging.debug('response: success')

        return HttpResponse('success')

