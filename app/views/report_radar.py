import logging
import json
import traceback
import copy
import csv

from django.db.models import Count, Min, Max, Q, Sum, Avg
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from app.views.helper import is_admin_access, get_status_text
from app.models import Event, Evaluation, Evaluator, Car, Benchmark


class ReportRadar(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ReportRadar, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        # Get Query String
        event_id = request.GET.get('event_id', None)
        if event_id is None:
            return JsonResponse({})

        event = Event.objects.filter(id=event_id).first()
        if event is None:
            return JsonResponse({})

        response = get_report(event)

        logging.debug('response: {0}'.format(response))
        return JsonResponse(response)


class CSVRadar(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CSVRadar, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        # Get Query String - Event ID
        event_id = request.GET.get('event_id', None)
        if event_id is None:
            return HttpResponse('invalid request', status=400)

        event = Event.objects.filter(id=event_id).first()
        if event is None:
            return HttpResponse('invalid request', status=400)

        # Get Query String - Evaluation ID
        evaluation_id = request.GET.get('evaluation_id', None)
        if evaluation_id is None:
            return HttpResponse('invalid request', status=400)

        evaluation = event.evaluations.filter(id=evaluation_id).first()
        if evaluation is None:
            return HttpResponse('invalid request', status=400)

        ################################################################################### Get Data Source

        data = event.get_admin_event_form()
        csv_data = self.get_csv_format(data, evaluation)

        ################################################################################### CSV
        file_name = 'radar_event_{}_evaluation_{}.csv'.format(event.code, evaluation.name)
        print('file_name:', file_name)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{0}"'.format(file_name)
        response['file_name'] = file_name

        writer = csv.writer(response)
        for data in csv_data:
            writer.writerow(data)

        return response

    def get_csv_format(self, data, evaluation_model):

        csv_data = []

        for evaluation in data['evaluations']:
            if evaluation_model.id != evaluation['id']:
                continue

            car_brand_row = [] + [''] + ['{} {}'.format(car['brand'], car['model']) for car in data['cars']]
            csv_data.append(car_brand_row)

            for criteria in evaluation['criteria']:
                criteria_point_row = [] + [criteria['name']] + [car['car_avg'] / 1.0 if (car['car_avg'] and car['car_avg'] != 'n/a') else car['car_avg'] for car in criteria['cars']]
                csv_data.append(criteria_point_row)

        return csv_data

# radar = event > evaluations > cars > criteria > radar chart data

def get_report(event):
    event_form = event.to_list()

    evaluation_list = event_form.get('evaluations', [])
    for evaluation in evaluation_list:

        car_list = copy.deepcopy(event.get_car_list())
        evaluation['cars'] = car_list
        for car in car_list:

            criteria_list = copy.deepcopy(evaluation['criteria'])
            car['criteria'] = criteria_list
            for criteria in criteria_list:

                event_id = event.id
                evaluation_id = evaluation['id']
                car_id = car['id']
                criteria_id = criteria['id']

                benchmark = Benchmark.objects.filter(event_id=event_id).filter(evaluation_id=evaluation_id).filter(car_id=car_id).filter(
                    criteria_id=criteria_id).exclude(point=None).exclude(disable=True).values('point').aggregate(
                    point_avg=Avg('point'))

                if benchmark['point_avg']:
                    point_avg = float(benchmark['point_avg']) if benchmark['point_avg'] else 0
                    point = float("{0:.2f}".format(point_avg))
                    radar_point = float("{0:.2f}".format(point_avg / 1.0))
                else:
                    point_avg = 'n/a'
                    point = 'n/a'
                    radar_point = 'n/a'

                criteria['point'] = point
                criteria['radar_point'] = radar_point

        # Make summary data

        summary = {}
        criteria_list = copy.deepcopy(evaluation['criteria'])
        for criteria in criteria_list:

            car_criteria_point = []
            car_list = copy.deepcopy(event.get_car_list())
            for car in car_list:

                benchmark = Benchmark.objects.filter(event_id=event.id).filter(evaluation_id=evaluation['id']).filter(car_id=car['id']).filter(
                    criteria_id=criteria['id']).exclude(point=None).exclude(disable=True).values('point').aggregate(
                    point_avg=Avg('point'))
                # point_avg = float(benchmark['point_avg']) if benchmark['point_avg'] else 0
                # point_avg = float("{0:.2f}".format(point_avg))

                if benchmark['point_avg']:
                    point_avg = float(benchmark['point_avg']) if benchmark['point_avg'] else 0
                    car_criteria_point.append({'car': car, 'criteria': criteria, 'point_avg': point_avg})
                else:
                    car_criteria_point.append({'car': car, 'criteria': criteria, 'point_avg': 'n/a'})

            car_criteria_point = sorted(car_criteria_point, key=lambda k: k['point_avg'] if k['point_avg'] != 'n/a' else 0, reverse=True)
            if len(car_criteria_point):
                prefered_car = car_criteria_point[0]

                car_key = '{} {}'.format(prefered_car['car']['brand'], prefered_car['car']['model'])
                cretiria_name = criteria['name']
                if car_key not in summary:
                    summary[car_key] = []
                summary[car_key].append(cretiria_name)

        evaluation['summary'] = summary

    logging.debug('event_form: {}'.format(event_form))
    return event_form
