import logging
import json
import traceback
import copy
import csv

from django.db.models import Count, Min, Max, Q, Sum, Avg
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from app.views.helper import is_admin_access, get_status_text
from app.models import Event, Evaluation, Evaluator, Car, Benchmark


class CSVSummary(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CSVSummary, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        # Get Query String - Event ID
        event_id = request.GET.get('event_id', None)
        if event_id is None:
            return HttpResponse('invalid request', status=400)

        event = Event.objects.filter(id=event_id).first()
        if event is None:
            return HttpResponse('invalid request', status=400)

        # Get Query String - Evaluation ID
        evaluation_id = request.GET.get('evaluation_id', None)
        if evaluation_id is None:
            return HttpResponse('invalid request', status=400)

        evaluation = event.evaluations.filter(id=evaluation_id).first()
        if evaluation is None:
            return HttpResponse('invalid request', status=400)

        ################################################################################### Get Data Source

        data = event.get_admin_event_form()
        csv_data = self.get_csv_format(data, evaluation)

        ################################################################################### CSV
        file_name = 'summary_event_{}_evaluation_{}.csv'.format(event.code, evaluation.name)
        print('file_name:', file_name)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{0}"'.format(file_name)
        response['file_name'] = file_name

        writer = csv.writer(response)
        for data in csv_data:
            writer.writerow(data)

        return response

    def get_csv_format(self, data, evaluation_model):

        csv_data = []

        for evaluation in data['evaluations']:
            if evaluation_model.id != evaluation['id']:
                continue

            for criteria in evaluation['criteria']:

                car_brand_row = [] + [criteria['name']] + ['{} {}'.format(car['brand'], car['model']) for car in criteria['cars']] + ['comment({} {})'.format(car['brand'], car['model']) for car in criteria['cars']]
                csv_data.append(car_brand_row)

                average_row = [] + ['average'] + [car['car_avg'] for car in criteria['cars']] + ['' for car in criteria['cars']]
                csv_data.append(average_row)

                for evaluator in criteria['evaluators']:
                    car_point = ['{} {} {}'.format(evaluator['code'], evaluator['first_name'], evaluator['last_name'])]  + [(car['benchmark']['point'] if (car['benchmark']['point'] and car['benchmark']['disable'] is False) else 'n/a') for car in evaluator['cars']] + [car['benchmark']['comment']  for car in evaluator['cars']]
                    csv_data.append(car_point)

        return csv_data
