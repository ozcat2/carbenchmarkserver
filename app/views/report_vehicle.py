import logging
import json
import traceback
import copy
import csv

from django.db.models import Count, Min, Max, Q, Sum, Avg
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from app.views.helper import is_admin_access, get_status_text
from app.models import Event, Evaluation, Evaluator, Car, Benchmark


BASE_MODEL = Event


class ReportVehicle(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ReportVehicle, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        # Get Query String
        event_id = request.GET.get('event_id', None)
        if event_id is None:
            return JsonResponse({})

        event = Event.objects.filter(id=event_id).first()
        if event is None:
            return JsonResponse({})

        response = get_report(event)

        logging.debug('response: {0}'.format(response))
        return JsonResponse(response)


class CSVVehicle(generic.View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CSVVehicle, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logging.debug('request: {0}'.format(self.request.GET))

        if not is_admin_access(request):
            logging.debug('Unauthorized')
            return HttpResponse('Unauthorized', status=401)

        # Get Query String - Event ID
        event_id = request.GET.get('event_id', None)
        if event_id is None:
            return HttpResponse('invalid request', status=400)

        event = Event.objects.filter(id=event_id).first()
        if event is None:
            return HttpResponse('invalid request', status=400)

        # # Get Query String - Evaluation ID
        # evaluation_id = request.GET.get('evaluation_id', None)
        # if evaluation_id is None:
        #     return HttpResponse('invalid request', status=400)
        #
        # evaluation = event.evaluations.filter(id=evaluation_id).first()
        # if evaluation is None:
        #     return HttpResponse('invalid request', status=400)

        # Get Query String - Car ID
        car_id = request.GET.get('car_id', None)
        if car_id is None:
            return HttpResponse('invalid request', status=400)

        car = Car.objects.filter(id=car_id).first()
        if car is None:
            return HttpResponse('invalid request', status=400)

        ################################################################################### Get Data Source

        data = get_report(event)
        csv_data = self.get_csv_format(data, car)

        ################################################################################### CSV
        file_name = 'vehicle_event_{}_vehicle_{}_{}.csv'.format(event.code, car.brand, car.model)
        print('file_name:', file_name)

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{0}"'.format(file_name)
        response['file_name'] = file_name

        writer = csv.writer(response)
        for data in csv_data:
            writer.writerow(data)

        return response

    def get_csv_format(self, data, car_model):

        csv_data = []

        for evaluation in data['evaluations']:
            # if evaluation_model.id != evaluation['id']:
            #     continue

            evaluation_head = ['{}'.format(evaluation['name'])]
            csv_data.append(evaluation_head)

            for car in evaluation['cars']:
                if car_model.id != car['id']:
                    continue

                # car_head = ['{} {}'.format(car['brand'], car['model'])]
                # csv_data.append(car_head)

                evaluator_row = [] + ['{} {}'.format(car['brand'], car['model'])] + [evaluator['code'] for evaluator in car['evaluators']]
                csv_data.append(evaluator_row)

                for criteria in car['criteria']:
                    criteria_point = [] + [criteria['name']] + [(evaluator['benchmark']['point'] if (evaluator['benchmark']['point'] and evaluator['benchmark']['disable'] is False) else 'n/a') for evaluator in criteria['evaluators']]
                    csv_data.append(criteria_point)

        return csv_data

    # def get_report(self, 'e'vent):
    #     event_form = event.to_list()
    #
    #     evaluation_list = event_form.get('evaluations', [])
    #     for evaluation in evaluation_list:
    #
    #         car_list = event.get_car_list()
    #         evaluation['cars'] = car_list
    #         for car in car_list:
    #
    #             criteria_list = evaluation['criteria']
    #             car['criteria'] = criteria_list
    #             for criteria in criteria_list:
    #
    #                 evaluator_list = event.get_evaluator_list()
    #                 criteria['evaluators'] = evaluator_list
    #
    #                 # For Average Calculator
    #                 point_sum = 0
    #                 num_evaluator = 0
    #
    #                 for evaluator in evaluator_list:
    #
    #                     event_id = event_form['id']
    #                     evaluation_id = evaluation['id']
    #                     criteria_id = criteria['id']
    #                     car_id = car['id']
    #                     evaluator_id = evaluator['id']
    #
    #                     benchmark = Benchmark.get_or_create_benchmark_from_query_id(evaluator_id,
    #                                                                                 event_id,
    #                                                                                 evaluation_id,
    #                                                                                 criteria_id,
    #                                                                                 car_id)
    #                     evaluator['benchmark'] = benchmark.to_list()
    #
    #                     if benchmark.point:
    #                         point_sum += float(benchmark.point)
    #                         num_evaluator += 1
    #
    #                 point_average = (point_sum / num_evaluator) if num_evaluator > 0 else 0
    #                 evaluator_average = {
    #                     'code': 'Average',
    #                     'benchmark': {
    #                         'point': point_average
    #                     }
    #                 }
    #                 evaluator_list.append(evaluator_average)
    #
    #     logging.debug('event_form: {}'.format(event_form))
    #     return event_form


def get_report(event):
    event_form = event.to_list()

    evaluation_list = event_form.get('evaluations', [])
    for evaluation in evaluation_list:

        car_list = event.get_car_list()
        evaluation['cars'] = car_list
        for car in car_list:

            evaluator_list = copy.deepcopy(event.get_evaluator_list())
            car['evaluators'] = evaluator_list
            for evaluator in evaluator_list:

                criteria_list = copy.deepcopy(evaluation['criteria'])
                evaluator['criteria'] = criteria_list
                for criteria in criteria_list:

                    event_id = event_form['id']
                    evaluation_id = evaluation['id']
                    criteria_id = criteria['id']
                    car_id = car['id']
                    evaluator_id = evaluator['id']

                    benchmark = Benchmark.get_or_create_benchmark_from_query_id(evaluator_id,
                                                                                event_id,
                                                                                evaluation_id,
                                                                                criteria_id,
                                                                                car_id)
                    criteria['benchmark'] = benchmark.to_list()

            # Make fake evaluator for Average Field
            evaluator_average = {
                'code': 'Average',
                'criteria': copy.deepcopy(criteria_list)
            }
            for criteria in evaluator_average['criteria']:

                event_id = event_form['id']
                evaluation_id = evaluation['id']
                criteria_id = criteria['id']
                car_id = car['id']

                benchmark = Benchmark.objects.filter(event_id=event_id).filter(evaluation_id=evaluation_id).filter(criteria_id=criteria_id).filter(car_id=car_id).exclude(point=None).exclude(disable=True).values('point').aggregate(point_avg=Avg('point'))
                if benchmark['point_avg']:
                    point_avg = float(benchmark['point_avg']) if benchmark['point_avg'] else 0
                    criteria['benchmark']['point'] = float("{0:.2f}".format(point_avg))
                else:
                    point_avg = 'n/a'
                    criteria['benchmark']['point'] = 'n/a'

            car['evaluators'].append(evaluator_average)

            # Criteria Side Object
            criteria_list = copy.deepcopy(evaluation['criteria'])
            car['criteria'] = criteria_list
            for criteria in criteria_list:

                evaluator_list = copy.deepcopy(event.get_evaluator_list())
                criteria['evaluators'] = evaluator_list
                for evaluator in evaluator_list:

                    event_id = event_form['id']
                    evaluation_id = evaluation['id']
                    criteria_id = criteria['id']
                    car_id = car['id']
                    evaluator_id = evaluator['id']

                    benchmark = Benchmark.get_or_create_benchmark_from_query_id(evaluator_id,
                                                                                event_id,
                                                                                evaluation_id,
                                                                                criteria_id,
                                                                                car_id)

                    evaluator['benchmark'] = benchmark.to_list()

                # Make fake evaluator for Average Field
                evaluator_average = {
                    'code': 'Average',
                    'benchmark': {
                        'point': 'n/a',
                        'disable': False
                    }
                }

                event_id = event_form['id']
                evaluation_id = evaluation['id']
                criteria_id = criteria['id']
                car_id = car['id']

                benchmark = Benchmark.objects.filter(event_id=event_id).filter(evaluation_id=evaluation_id).filter(
                    criteria_id=criteria_id).filter(car_id=car_id).exclude(point=None).exclude(disable=True).values('point').aggregate(
                    point_avg=Avg('point'))
                if benchmark['point_avg']:
                    point_avg = float(benchmark['point_avg']) if benchmark['point_avg'] else 0
                    evaluator_average['benchmark']['point'] = float("{0:.2f}".format(point_avg))
                else:
                    point_avg = 'n/a'
                    evaluator_average['benchmark']['point'] = 'n/a'

                criteria['evaluators'].append(evaluator_average)

    logging.debug('event_form: {}'.format(event_form))
    return event_form
