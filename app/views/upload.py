from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def simple_upload(request):
    if request.method in ['POST', 'PUT'] and request.FILES['file']:
        myfile = request.FILES['file']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)

        response = {
            'message': 'success',
            'logo': uploaded_file_url,
            'path': uploaded_file_url
        }

        return JsonResponse(response, status=200)

    return HttpResponse('fail', status=400)
