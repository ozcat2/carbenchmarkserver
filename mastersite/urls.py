# Copyright 2015 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from django.urls import path, re_path
from django.conf.urls.static import static

from django.views.static import serve

from app.views import index

from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView


urlpatterns = [
    url(r'^api/', include('app.urls')),
    re_path('admin*', TemplateView.as_view(template_name='index.html')),
    url(
            r'^favicon.ico$',
            RedirectView.as_view(
                url=staticfiles_storage.url('favicon.ico'),
                permanent=False),
            name="favicon"
        ),
    url(r'^application/', admin.site.urls, name='application'),
    url(r'^index/', index.index, name='index'),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}, name='static'),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}, name='media'),
    # url(r'^lib/(?P<path>.*)$', serve, {'document_root': settings.LIB_ROOT}, name='lib'),
    # url(r'^iconfont/(?P<path>.*)$', serve, {'document_root': settings.ICONFONT_ROOT}, name='lib'),
    # url(r'^(?P<path>.*)$', serve, {'document_root': settings.ABS_ROOT}, name='abs'),
    url(r'^$', RedirectView.as_view(url='/admin/', permanent=False), name='home'),
]

# This enables static files to be served from the Gunicorn server
# In Production, serve static files from Google Cloud Storage or an alternative
# CDN
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # urlpatterns += static(settings.LIB_URL, document_root=settings.LIB_ROOT)
    # urlpatterns += static(settings.ICONFONT_URL, document_root=settings.ICONFONT_ROOT)
    # urlpatterns += static(settings.ABS_URL, document_root=settings.ABS_ROOT)
