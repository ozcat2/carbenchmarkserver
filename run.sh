#!/usr/bin/env bash
sleep 10

brew services restart mysql
brew services start mysql

sleep 10

cd /Users/omsubusmo/Documents/ozcat/car_benchmark/car_benchmark

# git fetch
# git checkout prod
# git pull

source env/bin/activate

rm -rf static
python manage.py collectstatic

gunicorn --bind=0.0.0.0:8001 --workers=3 --reload --daemon mastersite.wsgi

# python manage.py runserver 0.0.0.0:8001
